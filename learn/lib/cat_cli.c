#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#define BUFSIZE 1024
char buffer[BUFSIZE];
int cat(int argc, char *argv[])
{
    int fd = 0; // 0=>STDIN_FILENO
    ssize_t read_bytes;
    while ((read_bytes = read(fd, buffer, BUFSIZE)))
    {
        write(1, buffer, read_bytes);
    }
    return 0;
}
