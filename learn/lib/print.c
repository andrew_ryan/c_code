#include <unistd.h>      //for solution0
#include <sys/syscall.h> //for solution1
#include <string.h>      // for stelen()

/*
solution0 print_str_0(char str[])
*/
int print_str_0(char str[])
{
    int result = write(1, str, strlen(str));
    return result;
}

/*
other solution print_str_1(char str[])
*/
int print_str_1(char str[])
{
    int result = syscall(SYS_write, 1, str, strlen(str));
    return result;
}
