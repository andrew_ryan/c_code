# useage:
# sh run.sh main.c
gcc -c $1 -o ./target/$1.o
gcc ./target/*.o -o ./target/run

# --static return huge symbols table  
# gcc --static ./target/*.o -o ./target/run
./target/run